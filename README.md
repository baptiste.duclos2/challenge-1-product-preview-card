# Frontend Mentor - Product preview card component solution

This is a solution to the [Product preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/product-preview-card-component-GO7UmttRfa). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover and focus states for interactive elements

### Screenshot
### DESKTOP VERSION
![desktop](Project/screenshots/desktop.png)
### MOBILE VERSION
![mobile](Project/screenshots/mobile.png)


## My process

- I began to cut the project in different part and started developing each part of my cuts
- Background of my page
- box of the main content
- image
- text content box

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid


### What I learned

I'm currently in the beggining of my journey as a Front developer but i did had some basics about web developpement as a web software engineer,
but i had a lot of struggle with the placement of my blocs and most importantly, the placement and understanding of the behavior of my image.
With a bit of reseach, i found the "contain:content" solution that really made my life easier with this project and i'm really glad i learned that.

```css
.maincontainer{
        margin: auto;
        contain: content;
    }
```

### Continued development

In future projects, I'll spend more time on the undestanding of flex so that I will no longer need any search on the internet.

